## Macro title: Report Page Problems 
## Body processing: N
##
## Developed by: Stephen Deutsch
##
## This macro adds a button to the page for users to report a deficiency in the page.
## This macro should be embedded in either the space header (for a particular space) or in the global header (for all pages).

## Copyright 2014 zanox AG
## 
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
## 
##     http://www.apache.org/licenses/LICENSE-2.0
## 
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.

## @noparams

#set ($loadMovePage = false)

#foreach ( $labelling in $content.getLabellings() )
  #set ( $label = $labelling.getLabel() )
  #set ( $group = "confluence-administrators" )
  #set ( $userName = $req.getRemoteUser().toString() )
  #if ( ( $label == "wrong-info" ) )
    <div style="width:800px; margin:0 auto;background-color:#F6F6F6;border: 1px solid white;">
      <table>
        <tbody>
          <tr>
            <td style="width:10px;background-color:orange;"></td>
            <td style="border:none;padding-left:10px;padding-right:5px;"><img src="https://kb.zanox.com/download/attachments/20710376/cleanup.png" /></td>
            <td style="border:none;padding-left:5px;padding-right:10px;">This page may require cleanup to meet the Knowledge Base quality standards. The specific problem is: <br />
            <strong>This page contains incorrect information.</strong> 
			#if($permissionHelper.canEdit($userAccessor.getUserIfAvailable($req.getRemoteUser()),$content))
			  <p>Please help <a href="$req.contextPath/pages/editpage.action?pageId=$content.getIdAsString()">improve this page</a> if you can, or <a href="#" onclick="AJS.$('div.quick-comment-prompt').click();">leave a comment</a>.</p>
			#end
			#if ( $userAccessor.hasMembership($group, $userName) )
			  <p> Reported by: $labelling.getOwningUser().getName() </p>
			#end
			</td>
          </tr>
        </tbody>
      </table>
    </div>
  #end
  
  #if ( ( $label == "needs-info" ) )
    <div style="width:800px; margin:0 auto;background-color:#F6F6F6;border: 1px solid white;">
      <table>
        <tbody>
          <tr>
            <td style="width:10px;background-color:orange;"></td>
            <td style="border:none;padding-left:10px;padding-right:5px;"><img src="https://kb.zanox.com/download/attachments/20710376/cleanup.png" /></td>
            <td style="border:none;padding-left:5px;padding-right:10px;">This page may require cleanup to meet the Knowledge Base quality standards. The specific problem is: <br />
            <strong>This page needs more information.</strong> 
			#if($permissionHelper.canEdit($userAccessor.getUserIfAvailable($req.getRemoteUser()),$content))
			  <p>Please help <a href="$req.contextPath/pages/editpage.action?pageId=$content.getIdAsString()">improve this page</a> if you can, or <a href="#" onclick="AJS.$('div.quick-comment-prompt').click();">leave a comment</a>.</p>
			#end
			#if ( $userAccessor.hasMembership($group, $userName) )
			  <p> Reported by: $labelling.getOwningUser().getName() </p>
			#end
			</td>
          </tr>
        </tbody>
      </table>
    </div>
  #end

  #if ( ( $label == "unclear-content" ) )
    <div style="width:800px; margin:0 auto;background-color:#F6F6F6;border: 1px solid white;">
      <table>
        <tbody>
          <tr>
            <td style="width:10px;background-color:orange;"></td>
            <td style="border:none;padding-left:10px;padding-right:5px;"><img src="https://kb.zanox.com/download/attachments/20710376/cleanup.png" /></td>
            <td style="border:none;padding-left:5px;padding-right:10px;">This page may require cleanup to meet the Knowledge Base quality standards. The specific problem is: <br />
            <strong>This page contains unclear content.</strong> 
			#if($permissionHelper.canEdit($userAccessor.getUserIfAvailable($req.getRemoteUser()),$content))
			  <p>Please help <a href="$req.contextPath/pages/editpage.action?pageId=$content.getIdAsString()">improve this page</a> if you can, or <a href="#" onclick="AJS.$('div.quick-comment-prompt').click();">leave a comment</a>.</p>
			#end
			#if ( $userAccessor.hasMembership($group, $userName) )
			  <p> Reported by: $labelling.getOwningUser().getName() </p>
			#end
			</td>
          </tr>
        </tbody>
      </table>
    </div>
  #end

  #if ( ( $label == "need-translation" ) )
    <div style="width:800px; margin:0 auto;background-color:#F6F6F6;border: 1px solid white;">
      <table>
        <tbody>
          <tr>
            <td style="width:10px;background-color:orange;"></td>
            <td style="border:none;padding-left:10px;padding-right:5px;"><img src="https://kb.zanox.com/download/attachments/20710376/cleanup.png" /></td>
            <td style="border:none;padding-left:5px;padding-right:10px;">This page may require cleanup to meet the Knowledge Base quality standards. The specific problem is: <br />
            <strong>This page needs to be translated (into English).</strong> 
			#if($permissionHelper.canEdit($userAccessor.getUserIfAvailable($req.getRemoteUser()),$content))
			  <p>Please help <a href="$req.contextPath/pages/editpage.action?pageId=$content.getIdAsString()">improve this page</a> if you can.</p>
			#end
			#if ( $userAccessor.hasMembership($group, $userName) )
			  <p> Reported by: $labelling.getOwningUser().getName() </p>
			#end
			</td>
          </tr>
        </tbody>
      </table>
    </div>
  #end

  #if ( ( $label == "wrong-place" ) )
    <div style="width:800px; margin:0 auto;background-color:#F6F6F6;border: 1px solid white;">
      <table>
        <tbody>
          <tr>
            <td style="width:10px;background-color:#F2D630;"></td>
            <td style="border:none;padding-left:10px;padding-right:5px;"><img src="https://kb.zanox.com/download/attachments/20710376/note.png" /></td>
            <td style="border:none;padding-left:5px;padding-right:10px;">This page may require action to meet the Knowledge Base quality standards. The specific problem is: <br />
            <strong>This page is in the wrong location.</strong> 
			#if($permissionHelper.canRemove($userAccessor.getUserIfAvailable($req.getRemoteUser()),$content))
			  <p>Please <a class="action-move-page-dialog-link-number2" href="/pages/viewpage.action?pageId=$content.getIdAsString()">
                <span>move</span></a> it to the proper place and delete "wrong-place" label.</p>
			#end
			#if ( $userAccessor.hasMembership($group, $userName) )
			  <p> Reported by: $labelling.getOwningUser().getName() </p>
			#end
			</td>
          </tr>
        </tbody>
      </table>
    </div>
	#set ($loadMovePage = true)
  #end

  #if ( ( $label == "to-archive" ) )
    <div style="width:800px; margin:0 auto;background-color:#F6F6F6;border: 1px solid white;">
      <table>
        <tbody>
          <tr>
            <td style="width:10px;background-color:#F2D630;"></td>
            <td style="border:none;padding-left:10px;padding-right:5px;"><img src="https://kb.zanox.com/download/attachments/20710376/note.png" /></td>
            <td style="border:none;padding-left:5px;padding-right:10px;">This page may require action to meet the Knowledge Base quality standards. The specific problem is: <br />
            <strong>This page should be archived.</strong> 
			#if($permissionHelper.canRemove($userAccessor.getUserIfAvailable($req.getRemoteUser()),$content))
			  <p>Please <a class="action-move-page-dialog-link-number2" href="/pages/viewpage.action?pageId=$content.getIdAsString()">
                <span>move</span></a> it to an archive folder and delete "to-archive" label.</p>
			#end
			#if ( $userAccessor.hasMembership($group, $userName) )
			  <p> Reported by: $labelling.getOwningUser().getName() </p>
			#end
			</td>
          </tr>
        </tbody>
      </table>
    </div>
	#set ($loadMovePage = true)
  #end

  #if ( ( $label == "can-delete" ) )
    <div style="width:800px; margin:0 auto;background-color:#F6F6F6;border: 1px solid white;">
      <table>
        <tbody>
          <tr>
            <td style="width:10px;background-color:#E63333;"></td>
            <td style="border:none;padding-left:10px;padding-right:5px;"><img src="https://kb.zanox.com/download/attachments/20710376/warning.png" /></td>
            <td style="border:none;padding-left:5px;padding-right:10px;">This page may require action to meet the Knowledge Base quality standards. The specific problem is: <br />
            <strong>This page should be deleted.</strong> 
			#if($permissionHelper.canRemove($userAccessor.getUserIfAvailable($req.getRemoteUser()),$content))
			  <p>Click <a href="https://kb.zanox.com/pages/removepage.action?pageId=$content.getIdAsString()">here</a> to delete this page.</p>
			#end
			#if ( $userAccessor.hasMembership($group, $userName) )
			  <p> Reported by: $labelling.getOwningUser().getName() </p>
			#end
			</td>
          </tr>
        </tbody>
      </table>
    </div>
  #end
#end

#if($permissionHelper.canEdit($userAccessor.getUserIfAvailable($req.getRemoteUser()),$content))
<script type="text/javascript">
#if ($loadMovePage == true)
try {
AJS.toInit(function(g){var h=840;var e=590;var f=71;Confluence.MovePageDialog=function(k){var p=AJS.Meta.get("page-title");k=g.extend({spaceKey:AJS.Meta.get("space-key"),spaceName:AJS.Meta.get("space-name"),pageTitle:p,parentPageTitle:AJS.Meta.get("parent-page-title"),title:AJS.format("Move Page \u2013 \u2018{0}\u2019",p),buttonName:"Move",openedPanel:"Advanced",moveHandler:function(I){AJS.log("No move handler defined. Closing dialog.");I.remove()},cancelHandler:function(I){I.remove();return false}},k);var z={spaceKey:k.spaceKey,spaceName:k.spaceName,parentPageTitle:k.parentPageTitle};var j=k.spaceKey;var B=k.spaceName;var F=k.parentPageTitle;var n="";var q="";var o=function(I,J){n=I;q=J};var G=AJS.ConfluenceDialog({width:h,height:e,id:"move-page-dialog"});G.addHeader(k.title);G.addPanel("Advanced",AJS.renderTemplate("movePageDialog"),"location-panel","location-panel-id");G.addPanel("Search",AJS.renderTemplate("movePageSearchPanel"),"search-panel","search-panel-id");G.addPanel("Recently Viewed",Confluence.Templates.MovePage.historyPanel({pageTitle:AJS.Meta.get("page-title")}),"history-panel","history-panel-id");G.addPanel("Browse",Confluence.Templates.MovePage.browsePanel({pageTitle:AJS.Meta.get("page-title")}),"browse-panel","browse-panel-id");G.get('#"'+"Advanced"+'"')[0].onselect=function(){g("#new-space-key").val(j);g("#new-space").val(B);g("#new-parent-page").val(F).select()};G.get('#"'+"Search"+'"')[0].onselect=function(){g("#move-page-dialog .search-panel .search-results .selected").removeClass("selected");g("#move-page-dialog input.search-query").focus()};G.get('#"'+"Recently Viewed"+'"')[0].onselect=function(){g(".history-panel",C).movePageHistory(u)};G.get('#"'+"Browse"+'"')[0].onselect=function(){AJS.log("browse: "+[j,B,F].join());g(".browse-panel",C).movePageBrowse(u,j,B,F,t,k.pageTitle)};var A=function(J){J.nextPage();var I=g("#move-page-dialog");g(".ordering-panel",I).movePageOrdering(j,F,k.pageTitle,o)};var x=function(J){var L=g("#new-space:visible").val();var K=g("#new-space-key").val();var I=g("#new-parent-page:visible").val();if(L&&(L!=B||K!=j||I!=F)){AJS.MoveDialog.getBreadcrumbs({spaceKey:K,pageTitle:I},function(){Confluence.PageLocation.set({spaceKey:K,spaceName:L,parentPageTitle:I});k.moveHandler(J,K,L,I,n,q,i)},function(M){g("#new-parent-breadcrumbs").html(Confluence.Templates.MovePage.breadcrumbError());if(M.status==404){u.error("The specified page was not found.")}})}else{Confluence.PageLocation.set({spaceKey:j,spaceName:B,parentPageTitle:F});k.moveHandler(J,j,B,F,n,q,i)}};var H=function(I){if(g("#reorderCheck")[0].checked){A(I)}else{x(I)}};G.addButton(k.buttonName,H,"move-button");G.addCancel("Cancel",k.cancelHandler);G.popup.element.find(".dialog-title").prepend(Confluence.Templates.MovePage.helpLink());G.addPage().addHeader(k.title).addPanel("Page Ordering",Confluence.Templates.MovePage.orderingPagePanel(),"ordering-panel","ordering-panel-id").addLink("Back",function(I){I.prevPage()},"dialog-back-link").addButton("Reorder",x,"reorder-button").addCancel("Cancel",k.cancelHandler);var D=G.get("button#"+k.buttonName)[0].item;g("button.move-button").before(Confluence.Templates.MovePage.reorderCheckbox());G.gotoPage(0);var C=g("#move-page-dialog");var y=C.find(".dialog-page-menu");var s=C.find(".dialog-page-body");var m=g(y[0]);var w=g(s[0]);w.height(m.height());w.width("75%");w.find(".dialog-panel-body").height(m.height()-f);var r=g(y[1]);var E=g(s[1]);r.width("0");E.width("100%");G.show();g(".location-panel .location-info",C).appendTo(g(".dialog-page-body:first",C));var l=new AJS.MoveDialog.Breadcrumbs(g("#new-parent-breadcrumbs"));function i(M){var L=g("#move-errors");if(L.length>0){L.remove()}L=g(Confluence.Templates.MovePage.errorMessage());var J=C.find(".browse-controls:visible");if(!J.length){J=C.find(".dialog-panel-body:visible")}J.append(L);if(!M||M.length==0){g(D).prop("disabled",false);return}var I="The following error(s) occurred:";I+="<ul>\n<li>";if(g.isArray(M)){I+=M.join("</li>\n<li>")}else{I+=M}I+="</li>\n</ul>";var K=atlassian.message.warning({content:I});L.html(K);L.removeClass("hidden");G.gotoPage(0)}var u={moveButton:D,clearErrors:function(){i([])},error:i,select:function(K,J,I){AJS.log("select: "+[K,J,I].join());j=K;B=J;F=I||"";g(D).prop("disabled",true);l.update({spaceKey:j,title:F},u)}};G.overrideLastTab();G.get('#"'+k.openedPanel+'"').select();var t=AJS.Meta.get("parent-page-title")||AJS.Meta.get("from-page-title");var v=new AJS.MoveDialog.Breadcrumbs(g("#current-parent-breadcrumbs"));v.update({spaceKey:AJS.Meta.get("space-key"),title:t},u);g(".location-panel",C).movePageLocation(u);g(".search-panel",C).movePageSearch(u);g(".history-panel",C).movePageHistory(u);g("#new-parent-page").select();if(k.hint){G.addHelpText(k.hint.template||k.hint.text,k.hint.arguments)}return C};var d=function(k,j,l,i){var m={pageId:AJS.params.pageId,spaceKey:k};if(l){m.position=i;m.targetId=l}else{if(j!=""){m.targetTitle=j;m.position="append"}else{m.position="topLevel"}}return m};function a(m,o,i,p,r,j,l){m=m.popup.element;m.addClass("waiting");g("button",m).prop("disabled",true);var k=g("<div class='throbber'></div>");m.append(k);var q=Raphael.spinner(k[0],100,"#666");function n(s){l(s);m.removeClass("waiting");q();g("button",m).prop("disabled",false)}g.ajax({url:contextPath+"/pages/movepage.action",type:"GET",dataType:"json",data:new d(o,p,r,j),error:function(){n("Move failed. There was a problem contacting the server.")},success:function(s){var t=[].concat(s.validationErrors||[]).concat(s.actionErrors||[]).concat(s.errorMessage||[]);if(t.length>0){n(t);return}window.location.href=contextPath+s.page.url+(s.page.url.indexOf("?")>=0?"&":"?")+"moved=true"}})}g("a.action-move-page-dialog-link-number2").click(function(i){i.preventDefault();if(g("#move-page-dialog").length>0){g("#move-page-dialog, body > .shadow, body > .aui-blanket").remove()}new Confluence.MovePageDialog({moveHandler:a});return false});var c;g("#rte-button-location").click(function(i){i.preventDefault();if(g("#move-page-dialog").length>0){g("#move-page-dialog, body > .shadow, body > .aui-blanket").remove()}new Confluence.MovePageDialog({spaceName:c,spaceKey:g("#newSpaceKey").val(),pageTitle:g("#content-title").val(),parentPageTitle:g("#parentPageString").val(),buttonName:"Move",title:"Set Page Location",moveHandler:function(n,k,o,p,m,l,j){c=o;g("#newSpaceKey").val(k);g("#parentPageString").val(p);if(p!=""){g("#position").val("append")}else{g("#position").val("topLevel")}if(m){g("#targetId").val(m);g("#position").val(l)}n.remove()}});return false});var b=null;Confluence.PageLocation={get:function(){if(b){return b}return{spaceName:AJS.Meta.get("space-name"),spaceKey:AJS.Meta.get("space-key"),parentPageTitle:AJS.Meta.get("parent-page-title")}},set:function(i){b=i}}});
} catch (err) {
    if (console && console.log && console.error) {
        console.log("Error running batched script.");
        console.error(err);
    }
}
#end

AJS.$(document).ready(function() {
	AJS.$("div#navigation > .ajs-menu-bar").append("<button class='aui-button aui-button-subtle aui-dropdown2-trigger' aria-owns='report-bad-page-dropdown' aria-haspopup='true'><span class='aui-icon aui-icon-error'>Error</span> Report this page</button><div id='report-bad-page-dropdown' class='aui-dropdown2 aui-style-default'><ul class='aui-list-truncate'><li><a id='wrong-info-link' href='$content.urlPath'>Page contains wrong information</a></li><li><a id='needs-info-link' href='$content.urlPath'>Page needs more information</a></li><li><a id='unclear-content-link' href='$content.urlPath'>Page content is unclear</a></li><li><a id='wrong-place-link' href='$content.urlPath'>Page needs to be moved</a></li><li><a id='need-translation-link' href='$content.urlPath'>Page needs to be translated</a></li><li><a id='should-archived-link' href='$content.urlPath'>Page should be archived</a></li><li><a id='can-delete-link' href='$content.urlPath'>Page can be deleted</a></li></ul></div>");
	AJS.$("a#wrong-info-link").attr("onclick", "jQuery.post(contextPath+'/json/addlabelactivity.action', {'entityIdString': AJS.params.contentId, 'labelString': 'wrong-info', 'atl_token': AJS.$('#atlassian-token').attr('content') });setTimeout(function(){ location.reload() }, 500);");
	AJS.$("a#needs-info-link").attr("onclick", "jQuery.post(contextPath+'/json/addlabelactivity.action', {'entityIdString': AJS.params.contentId, 'labelString': 'needs-info', 'atl_token': AJS.$('#atlassian-token').attr('content') });setTimeout(function(){ location.reload() }, 500);");
	AJS.$("a#unclear-content-link").attr("onclick", "jQuery.post(contextPath+'/json/addlabelactivity.action', {'entityIdString': AJS.params.contentId, 'labelString': 'unclear-content', 'atl_token': AJS.$('#atlassian-token').attr('content') });setTimeout(function(){ location.reload() }, 500);");
	AJS.$("a#need-translation-link").attr("onclick", "jQuery.post(contextPath+'/json/addlabelactivity.action', {'entityIdString': AJS.params.contentId, 'labelString': 'need-translation', 'atl_token': AJS.$('#atlassian-token').attr('content') });setTimeout(function(){ location.reload() }, 500);");
	AJS.$("a#wrong-place-link").attr("onclick", "jQuery.post(contextPath+'/json/addlabelactivity.action', {'entityIdString': AJS.params.contentId, 'labelString': 'wrong-place', 'atl_token': AJS.$('#atlassian-token').attr('content') });setTimeout(function(){ location.reload() }, 500);");
	AJS.$("a#should-archived-link").attr("onclick", "jQuery.post(contextPath+'/json/addlabelactivity.action', {'entityIdString': AJS.params.contentId, 'labelString': 'to-archive', 'atl_token': AJS.$('#atlassian-token').attr('content') });setTimeout(function(){ location.reload() }, 500);");
	AJS.$("a#can-delete-link").attr("onclick", "jQuery.post(contextPath+'/json/addlabelactivity.action', {'entityIdString': AJS.params.contentId, 'labelString': 'can-delete', 'atl_token': AJS.$('#atlassian-token').attr('content') });setTimeout(function(){ location.reload() }, 500);");
});
</script>
#end