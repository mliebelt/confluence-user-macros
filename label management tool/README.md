# Label Management Tool #

This tool allows you to change one label into another (rename/merge) or remove all labels with a particular name.  You can also restrict your action to a particular space.

Please be aware that you must have edit rights on the page in which the macro is placed to be able to use it.

## Features ##

- Rename all labels with a certain name
- Remove all labels with a certain name
- Split a label into multiple labels (simply enter desired labels with a space between each one)
- Merging labels is performed by renaming the wrong label name to the desired name.

## Parameters ##

Limit Results: As a default it is set to work with 1000 labels at a time. You can increase/decrease this by changing this number.

Default Space: The default is that this tool will search search all spaces (when the field is blank).  If you want it to default to a particular space, choose the space here. 


## Usage Instructions ##

After installing the user macro ([more information here](https://confluence.atlassian.com/display/DOC/Adding,+Editing+and+Removing+User+Macros)), insert it into a page.  After saving the page, the following dialog will appear on the page:

![user_guide1.png](http://i.imgur.com/0c6jnIi.png)


Type in the label you want to change (and optionally, the space you want to search), and then click "Search Label".  Then the following will appear:

![user_guide2.png](http://i.imgur.com/jX6F93F.png)

You can either:

- remove the label by clicking the "Remove Label" button 
- replace the label with one or more different labels by filling in the field and clicking "Replace Label"
- or you can start over on any screen by clicking the "Start Over" button.  

You can only perform an action if you have edit rights for the page that the label is embedded in.

**If you include an illegal character in the replace field, ( , !, #, &, (, ), \*, ,, ., :, ;, <, >, ?, @, [, ], ^) it will be removed automatically from your request.**

If you click on "Replace Label", you will see a warning letting you know that you can only undo your action one label at at time.  By clicking OK, you will see the following screen:

![user_guide3.png](http://i.imgur.com/rFNNsdg.png)

You will then see the results of the action, and you have an option to undo.  This is also useful for those situations in which you want to change all labels except for a few.  In that case, just change all labels, and on this screen you can undo just those few.

## Further Information ##

Since there is potential for abuse, consider making this macro available only to Confluence administrators.

This has not been thoroughly tested, but they should work at least on all versions from 5.0 - 5.4.4.

If you find any issues, please [let me know](mailto:stephen.deutsch@zanox.com).